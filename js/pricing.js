const pricing = () =>{
    const boxSwitch = document.querySelector('.box-switch')
    const slider = document.querySelector('.slider')
    const priceText = document.getElementById('price').innerText
    let price = parseInt(priceText)
    if(slider.classList.contains('move-slider')){
        boxSwitch.classList.remove('bg-blue')
        slider.classList.remove('move-slider')
        handleDecrease(price)
    }
    else {
        boxSwitch.classList.add('bg-blue')
        slider.classList.add('move-slider')
        handleIncrease(price)
    }
}

const handleIncrease = (price) =>{
    const handle1 = () =>{
        if(price == 49){
            clearInterval(autoIncreasePrice)
        }else{
            price++
            document.getElementById('price').innerText = price.toString()
            }
        }
    
    const autoIncreasePrice = setInterval(handle1, 30)
}

const handleDecrease = (price) =>{
    const handle2 = () =>{
        if(price == 29){
            clearInterval(autoDecreasePrice)
        }else{
            price--
            document.getElementById('price').innerText = price.toString()
            }
        }
    
    const autoDecreasePrice = setInterval(handle2, 30)
}
