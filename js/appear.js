const colWelcomes = document.querySelectorAll('.col-section-welcome')
const colFeatures = document.querySelectorAll('.col-feature')
const colAboutsAbove = document.querySelectorAll('.col-about')
const colAboutsBelow = document.querySelectorAll('.col-half')
const colPricings = document.querySelectorAll('.col-pricing')

const addEventAppear = (cols) =>{
    cols.forEach((col, index) =>{
        if(col.getBoundingClientRect().top < (window.innerHeight || document.documentElement.clientHeight)){
            col.classList.add('appear-section')
        }
    })
}

const listEvent = () =>{
    addEventAppear(colWelcomes)
    addEventAppear(colFeatures)
    addEventAppear(colAboutsAbove)
    addEventAppear(colAboutsBelow)
    addEventAppear(colPricings)

}

window.addEventListener('load', ()=>{ listEvent() })

window.addEventListener('scroll', ()=>{ listEvent() })
