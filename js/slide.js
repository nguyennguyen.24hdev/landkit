const image_1 = document.querySelector('.img-1')
const image_2 = document.querySelector('.img-2')

const cardBody = document.querySelector('.card-body')
const cardBody1 = document.querySelector('.card-body-1')
const cardBody2 = document.querySelector('.card-body-2')

const switchBackgroundImage = () =>{
    if(image_1.classList.contains('slide-show')){
        image_2.classList.add('slide-show')
        image_1.classList.remove('slide-show')
    }else{
        image_1.classList.add('slide-show')
        image_2.classList.remove('slide-show')
    }
}

const switchContentSlide = (hidden, show) =>{
    if(cardBody1.classList.contains('slide-show')){
        cardBody2.classList.add('slide-show')
        cardBody2.style = `animation: ${show} 0.6s ease`
        cardBody1.classList.remove('slide-show')
        cardBody1.style = `animation: ${hidden} 0.6s ease`
    }else{
        cardBody1.classList.add('slide-show')
        cardBody1.style = `animation: ${show} 0.6s ease`
        cardBody2.classList.remove('slide-show')
        cardBody2.style = `animation: ${hidden} 0.6s ease`
    }
}

const handleSwitchSlideLeft = () =>{
    switchBackgroundImage()
    switchContentSlide('slide-left-hidden', 'slide-left-show')
}

const handleSwitchSlideRight= () =>{
    switchBackgroundImage()
    switchContentSlide('slide-right-hidden', 'slide-right-show')
}