const colStats = document.querySelector('.section-stats .col-left')
const textSatisfaction = document.getElementById('text-satisfaction')
const suppport_1 = document.getElementById('item-support-1')
const suppport_2 = document.getElementById('item-support-2')
const textSales = document.getElementById('text-sales')

const runNumber = (max, element, step) =>{
    let value = 0
    const handle4 = () =>{
        if(value == max){
            clearInterval(count)
        }else{
            value++
            element.innerText = `${value}`
        }
    }
    const count = setInterval(handle4, step)
    colStats.classList.add('finishedRunNumber')
}

window.addEventListener('scroll', ()=>{
    if(!colStats.classList.contains('finishedRunNumber')){
        if(colStats.getBoundingClientRect().top < (window.innerHeight || document.documentElement.clientHeight)){
            runNumber(100, textSatisfaction, 5)
            runNumber(24, suppport_1, 30)
            runNumber(7, suppport_2, 40)
            runNumber(100, textSales, 5)
        }
    }
})
