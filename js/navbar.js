const navCollapse = document.querySelector('.navbar-collapse')
const navItemDrop = document.querySelectorAll('.nav-item-dropdown')
const body = document.querySelector('.bgcolor')
const listDrop = document.querySelectorAll('.box-dropdown')
const listIcon= document.querySelectorAll('.material-icons-outlined')

const handleOpenNav = () =>{
    navCollapse.classList.add('display-block')
    navItemDrop.forEach((dropdown, index)=>{
        dropdown.classList.toggle('visible')
    })

    body.classList.toggle('disable-scroll')
    document.body.style.backgroundColor = "#d6d2d233";
}

const handleCloseNav = ()=>{
    navCollapse.classList.remove('display-block')
    navItemDrop.forEach((dropdown, index)=>{
        dropdown.classList.remove('visible')
    })

    body.classList.remove('disable-scroll')
    document.body.style.backgroundColor = "white";
}


const openDrop = (className, classIcon)=>{
    const dropdown = document.querySelector(className)
    const icon = document.querySelector(classIcon)
    for (let i = 0; i < listDrop.length; i++) {
        if(listDrop[i].classList[1] != className.slice(1)){
            listDrop[i].classList.remove('open-dropdown-acc')
            listIcon[i].classList.remove('rotate-icon')
        }
    }
    if(dropdown.classList.contains('open-dropdown-acc')){
        dropdown.classList.remove('open-dropdown-acc')
        icon.classList.remove('rotate-icon')
    }else{
        dropdown.classList.add('open-dropdown-acc')
        icon.classList.add('rotate-icon')
    }
}

const handleOpenDropdown = (index) =>{
    switch (index) {
        case 0:
            openDrop('.dropdown-setting','.icon-0')
            break;
        case 1:
            openDrop('.dropdown-signin','.icon-1')
            break;
        case 2:
            openDrop('.dropdown-signup','.icon-2')
            break; 
        case 3:
            openDrop('.dropdown-pass','.icon-3')
            break;
        case 4:
            openDrop('.dropdown-error','.icon-4')
            break;
    }
}
