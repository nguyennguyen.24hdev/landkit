const textfieldName = document.getElementById('name')
const textfieldEmail = document.getElementById('email')
const textfieldPass = document.getElementById('password')
const notifiName = document.getElementById('notifi-name')
const notifiEmail = document.getElementById('notifi-email')
const notifiPass = document.getElementById('notifi-password')

const btnSubmit = document.getElementById('btn-submit')

const resetForm = () =>{
    textfieldName.value = ''
    textfieldEmail.value = ''
    textfieldPass.value = ''
}

const resetError = () =>{
    notifiName.value = ''
    notifiEmail.value = ''
    notifiPass.value = ''
}

const blurInput = () =>{
    textfieldName.blur()
    textfieldEmail.blur()
    textfieldPass.blur()
}

const validateName = () =>{
    if(textfieldName.value.length == 0){
        notifiName.innerText = 'Name is required'
        return false
    }else if(textfieldName.value.length >8){
        notifiName.innerText = 'Name must be less than 8 character'
        return false;
    }else{
        notifiName.innerText = '' 
        return true
    }
}

const validateEmail = () =>{
    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(textfieldEmail.value.length == 0){
        notifiEmail.innerText = 'Email is required'
        return false
    }else if(!textfieldEmail.value.match(mailFormat)){
        notifiEmail.innerText = 'Invalid email'
        return false
    }else{
        notifiEmail.innerText = ''
        return true
    }
}

const validatePass = () =>{
    if(textfieldPass.value.length == 0){
        notifiPass.innerText = 'Password is required'
        return false
    }else if(textfieldPass.value.includes(' ')){
        notifiPass.innerText = 'Password must be not contain space'
        return false
    }else if(textfieldPass.value.length >8){
        notifiPass.innerText = 'Password must be less than 8 character'
        return false;
    }else{
        notifiPass.innerText = ''
        return true
    }
}

btnSubmit.addEventListener('click', (e) =>{
    e.preventDefault()
    if(validateName() && validateEmail() && validatePass()){
        const formData = "Name: " + textfieldName.value + 
                        "\nEmail: " + textfieldEmail.value + 
                        "\nPassword: " + textfieldPass.value 
        resetForm()
        blurInput()
        console.log(formData);
    } else{
        console.log('Form validation error');
    }
})